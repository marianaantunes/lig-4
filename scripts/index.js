let tower1, tower2, tower3, tower4, tower5, tower6, tower7, totalTowers;
const columns = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];
const main = document.getElementById('mainContent');

const board = [];
for (let i = 1; i <=  6; i++) {
  for (let y = 0; y < columns.length; y++) {
    board.push(columns[y] + i);
  };
};

let winningCombinations = [];
for (let i = 0; i < columns.length - 3; i++) {
  for (let y = 1; y < 7; y++) {
    winningCombinations.push([columns[i] + y, columns[i + 1] + y, columns[i + 2] + y, columns[i + 3] + y]);
  }
}
for (let i = 0; i < columns.length; i++) {
  for (let y = 1; y < 4; y++) {
    winningCombinations.push([columns[i] + y, columns[i] + (y + 1), columns[i] + (y + 2), columns[i] + (y + 3)]);
  }
}
for (let i = 0; i < columns.length - 3; i++) {
  for (let y = 1; y < 4; y++) {
    winningCombinations.push([columns[i] + y, columns[i + 1] + (y + 1), columns[i + 2] + (y + 2), columns[i + 3] + (y + 3)]);
  };
};
for (let i = 0; i < columns.length - 3; i++) {
  for (let y = 6; y > 3; y--) {
    winningCombinations.push([columns[i] + y, columns[i + 1] + (y - 1), columns[i + 2] + (y - 2), columns[i + 3] + (y - 3)]);
  };
};

function createTowers() {
  tower1 = [board[0], board[7], board[14], board[21], board[28], board[35]];
  tower2 = [board[1], board[8], board[15], board[22], board[29], board[36]];
  tower3 = [board[2], board[9], board[16], board[23], board[30], board[37]];
  tower4 = [board[3], board[10], board[17], board[24], board[31], board[38]];
  tower5 = [board[4], board[11], board[18], board[25], board[32], board[39]];
  tower6 = [board[5], board[12], board[19], board[26], board[33], board[40]];
  tower7 = [board[6], board[13], board[20], board[27], board[34], board[41]];
  totalTowers = {
    A: tower1,
    B: tower2,
    C: tower3,
    D: tower4,
    E: tower5,
    F: tower6,
    G: tower7
  };
}

let currentPlayer;
let rounds;
let red;
let blue;
let redRounds;
let blueRounds;
let letters = {};

function start() {
  createTowers();
  currentPlayer = 'red';
  rounds = [];
  red = 'red';
  blue = 'blue';
  redRounds = [];
  blueRounds = [];
  letters = {};
  columns.forEach( letter => {
    letters[letter] = 5;
  });

  main.innerHTML = '';
  for (let i in board) {
    const div = document.createElement('div');
    div.id = board[i];
    div.className = 'tab ' + board[i];
    main.appendChild(div);
    div.addEventListener('click', swapPlayer);
  }
};

function swapPlayer(event){
  let id = event.target.id;
  switch (currentPlayer) {
    case red:
      rounds.push(currentPlayer);

      let redIndex = totalTowers[id[0]][ letters[id[0]] ].toString();
      let redElement = document.getElementById(redIndex);

      redRounds.push(totalTowers[ id[0] ][ letters[id[0]] ]);
      totalTowers[id[0]][ letters[id[0]] ] = (redElement.classList.add('red'));

      console.log(`teste jogadas VERMELHA ${id[0]}: ` + redRounds);

      isWinner(redRounds);
      currentPlayer = 'blue';
      return letters[id[0]] = letters[id[0]] - 1;

      break;

    case blue:
      rounds.push(currentPlayer);

      let blueIndex = totalTowers[id[0]][ letters[id[0]] ].toString();
      let blueElement = document.getElementById(blueIndex);

      blueRounds.push(totalTowers[ id[0] ][ letters[id[0]] ]);
      totalTowers[id[0]][ letters[id[0]] ] = (blueElement.classList.add('blue'));

      console.log(`teste jogadas azuis ${id[0]}: ` + blueRounds);

      isWinner(blueRounds);
      currentPlayer = 'red';
      return letters[id[0]] = letters[id[0]] - 1;

      break;

    default:
      break;
  }
}

function isWinner(colorRounds) {
  if (rounds.length === 42) {
    printDrawMessage();
  }
  for (let i = 0; i < winningCombinations.length; i++) {
    let test = winningCombinations[i].filter( (id) => {
      return colorRounds.includes(id);
    })
    if (test.length === 4) {
      printWinningMessage();
      return;
    }
  };
}

function printWinningMessage() {
  document.querySelectorAll('.tab').forEach( tab => {
    tab.removeEventListener('click', swapPlayer);
  });
  
  let div = document.createElement('div');
  div.setAttribute('class', 'message container');
  document.querySelector('body').appendChild(div);

  let message = document.createElement('p');
  message.classList.add('wonMessage')
  message.innerText = 'Color ' + currentPlayer + ' won!';
  div.appendChild(message);

  let restartBtn = document.createElement('button');
  restartBtn.classList.add('playAgain')
  restartBtn.innerText = 'Play again?';
  div.appendChild(restartBtn);
  restartBtn.addEventListener('click', () => {
    document.querySelector('main').innerHTML = '';
    div.remove();
    start();
  })
}

function printDrawMessage() {
  document.querySelectorAll('.tab').forEach( tab => {
    tab.removeEventListener('click', swapPlayer);
  });

  let div = document.createElement('div');
  div.setAttribute('class', 'message container');
  document.querySelector('body').appendChild(div);

  let message = document.createElement('p');
  message.classList.add('drawMessage')
  message.innerText = 'No one won this time!';
  div.appendChild(message);

  let restartBtn = document.createElement('button');
  restartBtn.classList.add('playAgain')
  restartBtn.innerText = 'Play again?';
  div.appendChild(restartBtn);
  restartBtn.addEventListener('click', () => {
    document.querySelector('main').innerHTML = '';
    div.remove();
    start();
  })
}

document.getElementById('start').addEventListener('click', start);
